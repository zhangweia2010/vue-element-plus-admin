import { PageListSchema } from '@/components-lowcode/types/lowcode'
 
import {reactive} from "vue"

const schema = reactive<PageListSchema>({

    isInDialog:true,
    searchConfig: {
        data: {
            dataSourceConfigId: 0,
            name: '',
            comment:"",
        },
        attrs: {

        },
        widgets: [
            {
                formItem: {
                },
                label: "数据源",
                key: "dataSourceConfigId",
                component: "ZwSelect",
                hidden: false,
                attrs: {
                    placeholder: "请选择数据源",
                    clearable: true,
                },
                options: [],
                optionsAlias: {
                    labelField: "name",
                    valueField: "id"
                },
                dataSource:"initNet", //initNet,local,dict
                ajax:{
                    method:"get",
                    url:"/infra/data-source-config/list",
                    params:{}
                }
            },
            {
                formItem: {
                },
                label: "表名称",
                key: "name",
                component: "ZwInput",
                hidden: false,
                attrs: {
                    placeholder: "请输入表名称",
                    clearable: true,
                },
            },
            {
                formItem: {
                },
                label: "表描述",
                key: "comment",
                component: "ZwInput",
                hidden: false,
                attrs: {
                    placeholder: "请输入表描述",
                    clearable: true,
                },
            }
        ]
    },
    tableConfig: {
        border: true,
        height:"360px",
        columnConfig: {
            resizable: true
        },
        rowConfig: {
            keyField: 'id',
            isHover: true
        },
        checkboxConfig: {
            highlight: true,
            showHeader: true,
        },
        columns: [
            { type: 'checkbox', title: '', width: 60, align: "center" },
            { field: 'name', title: '表名称',align: "center"},           
            { field: 'comment', title: '表描述',align: "center"},
        ],
        pagerConfig:{
            enabled:false
        },
        proxyConfig: {
            props: {
                result:"list"
            },
            ajax: {

            }
        },
    },

    proxyConfig: {
        query: {
            method: "get",
            url: "/infra/codegen/db/table/list",
            params: {
                default: [] //{key,value}
            }
        },
    },

})

export {schema}




