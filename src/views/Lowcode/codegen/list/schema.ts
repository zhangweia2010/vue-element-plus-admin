import { PageListSchema } from '@/components-lowcode/types/lowcode'
 
import {reactive} from "vue"

export function getSchema(refListPage){
    const schema = reactive<PageListSchema>({

        searchConfig: {
            data: {
                tableName: '',
                className: '',
            },
            attrs: {
    
            },
            widgets: [
                {
                    formItem: {
                    },
                    label: "表名称",
                    key: "tableName",
                    component: "ZwInput",
                    hidden: false,
                    attrs: {
                        placeholder: "请输入表名称",
                        clearable: true,
                    },
                    defaultValue: "",//默认值
                },
                {
                    formItem: {
                    },
                    label: "实体",
                    key: "className",
                    component: "ZwInput",
                    hidden: false,
                    attrs: {
                        placeholder: "请输入实体",
                        clearable: true,
                    },
                }
            ]
        },
        tableConfig: {
            border: true,
            columnConfig: {
                resizable: true
            },
            rowConfig: {
                keyField: 'id',
                isHover: true
            },
            checkboxConfig: {
                highlight: true,
                showHeader: true,
            },
            columns: [
                { type: 'seq', width: 50, align: "center"},
                { field: 'tableName', title: '表名称'},           
                { field: 'tableComment', title: '表描述'},
                { field: 'className', title: '表实体'},
                { field: 'createTime', title: '创建时间'},
                { field: 'updateTime', title: '更新时间'},
                { title: '操作', slots: { default: "slotOpts" }, align: "center" },
            ],
            opts: [
                {
                    title: '编辑', icon: 'ep:view',
                    event: (row) => {refListPage.value.emitEvent({action:"showEditTable", id:row[schema.tableConfig.rowConfig.keyField]})}
                },

                {
                    title: '同步', icon: 'ep:refresh', 
                    event: (row) => {refListPage.value.emitEvent({action:"syncTable", row:row})}
                },
                {
                    title: '生成', icon: 'ep:download',
                    event: (row) => {refListPage.value.emitEvent({action:"gencode", row:row})}
                },
                {
                    title: '删除', icon: 'ep:delete', type: "danger",
                    event: (row) => { refListPage.value.onDeleteItem(row) }
                },
            ],
            buttons: [
                {
                    code: 'itemDetail', title: '导入', icon: 'ep:zoom-in', type: "primary",
                    event: () => { refListPage.value.emitEvent({action:"showSelectTable"}) }
                },
            ],
            toolbarConfig: {
                slots: {
                    buttons: "slotButtons"
                },
                refresh: true,
                zoom: true,
            },
            proxyConfig: {
                props: {
                    result: 'list', // 配置响应结果列表字段
                    total: 'total' // 配置响应结果总页数字段
                },
                ajax: {
    
                }
            },
        },
    
        proxyConfig: {
            query: {
                method: "get",
                url: "/infra/codegen/table/page",
                params: {
                    default: [] //{key,value}
                }
            },
            delete: {
                method: "delete",
                url: "/infra/codegen/delete",
                params: {
                }
            },
        },
    
    })
    return schema
}




