import { reactive } from "vue";

export function getBaseInfoSchema(menuOptions) {
    const schema = reactive({
        data: {
            parentMenuId: "",
            tableName: "",
            tableComment: "",
            className: "",
            moduleName: "",
            businessName: "",
            classComment: "",
            remark: "",
            templateType: "",
            scene: "",
        },
        attrs: {
            inline: true,
            labelWidth: "140px",
        },
        widgets: [
            {
                formItem: {
                },
                label: "上级菜单",
                key: "parentMenuId",
                component: "ZwTreeSelect",
                hidden: false,
                attrs: {
                    props:{
                        children: 'children',
                        label: 'name',
                        value: 'id'
                    },
                    checkStrictly: true,
                    nodeKey: 'id'
                },
                
                options:menuOptions,
            },
            {
                formItem: {
                },
                label: "表名称",
                key: "tableName",
                component: "ZwInput",
                hidden: false,
                attrs: {
                },
            },
            {
                formItem: {
                },
                label: "表描述",
                key: "tableComment",
                component: "ZwInput",
                hidden: false,
                attrs: {
                },
            },
            {
                label: '实体类名称',
                key: 'className',
                component: 'ZwInput',
                hidden: false,
            },
            {
                label: '类名称',
                key: 'className',
                component: 'ZwInput',
                labelMessage: '类名称（首字母大写），例如SysUser、SysMenu、SysDictData 等等',
                hidden: false,
            },
            {
                label: '模块名',
                key: 'moduleName',
                component: 'ZwInput',
                labelMessage: '模块名，即一级目录，例如 system、infra、tool 等等',
                hidden: false,
            },
            {
                label: '业务名',
                key: 'businessName',
                component: 'ZwInput',
                labelMessage: '业务名，即二级目录，例如 user、permission、dict 等等',

            },
            {
                label: '类描述',
                key: 'classComment',
                component: 'ZwInput',
                labelMessage: '用作类描述，例如 用户',
            },
            {
                label: '备注',
                key: 'remark',
                component: 'ZwInput',
            }
        ],
        proxyConfig: {
        },
    })
    return schema
}