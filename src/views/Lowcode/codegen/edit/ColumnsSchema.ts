import {PageListEditSchema} from "@/components-lowcode/types/lowcode"
import {reactive} from "vue"
import request from '@/config/axios'

export  function getColumnsSchema(refListPage, dictOptions){
    const cmpOptions = [
        {label:"文本框", value:"input"},
        {label:"文本域", value:"textarea"},
        {label:"下拉框", value:"select"},
        {label:"单选框", value:"radio"},
        {label:"复选框", value:"checkbox"},
        {label:"日期控件", value:"datetime"},
    ]
    const queryOptions = [
        {label:"=", value:"="},
        {label:"!=", value:"!="},
        {label:">", value:">"},
        {label:">=", value:">="},
        {label:"<", value:"<"},
        {label:"<=", value:"<="},
        {label:"LIKE", value:"LIKE"},
        {label:"BETWEEN", value:"BETWEEN"},
    ]

    const schema = reactive<PageListEditSchema>({
        tableConfig: {
            border: true,
            keepSource: true,
            columnConfig: {
                resizable: true
            },
            rowConfig: {
                keyField: 'id',
                isHover: true
            },
            checkboxConfig: {
                highlight: true,
                showHeader: true,
            },
            pagerConfig:{
                enabled:false
            },
            columns: [
                { type: 'checkbox', title: '', width: 60, align: "center" },
                { field: 'columnName', title: '字段列名',editRender: { name: 'input', attrs: { placeholder: '请输入字段列名' } }},
                { field: 'dataType', title: '物理类型' ,editRender: { name: 'input', attrs: { placeholder: '请输入物理类型' } }},
                { field: 'javaField', title: 'Java类型' ,editRender: { name: 'input', attrs: { placeholder: '请输入Java类型' } }},
                { field: 'createOperation', title: '插入',width: 70, align:'center',  editRender: { name: '$switch', attrs: {} }},
                { field: 'updateOperation', title: '编辑',width: 70, align:'center', editRender: { name: '$switch', attrs: { } }},
                { field: 'listOperationResult', title: '列表', width: 70, align:'center', editRender: { name: '$switch', attrs: {  } }},
                { field: 'listOperation', title: '查询', width: 70, align:'center', editRender: { name: '$switch', attrs: {  } }},
                { field: 'nullable', title: '允许空', width: 100, align:'center', editRender: { name: '$switch', attrs: { } }},
                { field: 'listOperationCondition', title: '查询方式', editRender: { name: '$select', options:queryOptions,attrs: { placeholder: '请选择查询方式' } }},
                { field: 'htmlType', title: '显示类型', editRender: { name: '$select',  options:cmpOptions,attrs: { placeholder: '请选择显示类型' } }},
                { field: 'dictType', title: '字典类型', editRender: { name: '$select', options:dictOptions, attrs: { placeholder: '请选择字典类型' } }},
                { field: 'example', title: '默认值', editRender: { name: 'input', attrs: { placeholder: '请输入默认值' } }},
            ],
            proxyConfig: {
                autoLoad:false,
                ajax:{}
            },
            editConfig: {
                trigger: 'click',
                mode: 'row',
                showStatus: true
            }
        },
    
        proxyConfig: {
            query: {
                method: "get",
                url: "/system/user/page",
                params: {
                    default: [] //{key,value}
                }
            },
            delete: {
                method: "delete",
                url: "/system/user/delete",
                params: {
                }
            },
        },
    })
    return schema
}