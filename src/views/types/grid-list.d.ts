export type VxeGridListProps<D = any> = VxeTableProps<D> & {
    columns?: VxeGridPropTypes.Columns
    pagerConfig?: VxeGridPropTypes.PagerConfig
    proxyConfig?: VxeGridPropTypes.ProxyConfig
    toolbarConfig?: VxeGridPropTypes.ToolbarConfig
    searchConfig?: VxeGridPropTypes.FormConfig
    zoomConfig?: VxeGridPropTypes.ZoomConfig
    opts:any[]
  }