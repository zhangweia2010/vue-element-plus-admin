/*
 * @Description: 定义表单组件
 * @Author: zhangwei
 * @Date: 2023-03-14 22:37:32
 * @LastEditTime: 2023-03-18 08:15:57
 * @LastEditors: zhangwei
 */
import type { Component } from 'vue'

import { ZwInput } from "./input"
import { ZwSelect ,ZwTreeSelect} from "./select"
import { ZwButton, ZwTextButton } from "./button"
import { ZwLabel } from './label'
import { ZwCheckbox } from './check'

export type FormComponentName = 
  | 'ZwInput'
  | 'ZwSelect'
  | 'ZwTreeSelect'
  | 'ZwButton'
  | "ZwTextButton"
  | "ZwLabel"
  | "ZwCheckbox"


const formCmpMap: Recordable<Component, FormComponentName> = {
  ZwInput: ZwInput,
  ZwSelect: ZwSelect,
  ZwTreeSelect: ZwTreeSelect,
  ZwButton: ZwButton,
  ZwTextButton:ZwTextButton,
  ZwLabel: ZwLabel,
  ZwCheckbox:ZwCheckbox
}

export { formCmpMap };