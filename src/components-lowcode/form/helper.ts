import type { Slots } from 'vue'
import { getSlot } from '@/utils/tsxHelper'

/**
 * @param slots 插槽
 * @param field 字段名
 * @returns 返回FormIiem插槽
 */
export const setFormItemSlots = (slots: Slots, field: string): Recordable => {
    const slotObj: Recordable = {}
    if (slots[`${field}-error`]) {
      slotObj['error'] = (data: Recordable) => {
        return getSlot(slots, `${field}-error`, data)
      }
    }
    if (slots[`${field}-label`]) {
      slotObj['label'] = (data: Recordable) => {
        return getSlot(slots, `${field}-label`, data)
      }
    }
    return slotObj
  }