/*
 * @Description: 
 * @Author: zhangwei
 * @Date: 2023-03-15 20:31:33
 * @LastEditTime: 2023-03-18 22:22:02
 * @LastEditors: zw 775925302@qq.com
 */

import { computed } from "vue"

/**
 *   拿到el-form-item的属性设置
 * @param schema 
 * @returns 
 */
export function useFormItemAttr(schema:Recordable){
    var attr = {
        prop:schema.key
    }
    if (schema.label) {//针对input不需要显示label的场景
        attr['label'] = schema.label
    }
    if (schema.formItem?.labelWidth) {
        attr['labelWidth'] = schema.formItem.labelWidth
    }
    if (schema.formItem?.rules) {
        attr['rules'] = schema.formItem.rules
    }
    return attr
}



/**
 * 设置组件的modelValue值，这里需要传props对象才会修改，不能直接传modelValue的值
 * 解决v-model问题
 * @param props  
 * @param emit 
 * @returns 
 */
export function useFormItemModelValue(props: Recordable, emit) {

    return computed({
        get() {
            return props.modelValue
        },
        set(value) {
            emit('update:modelValue', value)
        }
    })
}

