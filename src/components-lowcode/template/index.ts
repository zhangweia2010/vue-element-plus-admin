import type { Component } from 'vue'

import { ZwTemplateListPage } from "./list"
import { ZwTemplateListEditPage } from "./list-edit"

export type PageComponentName = 
  | 'TemplateListPage'
  | 'TemplateListEditPage'


const templatePageMap: Recordable<Component, PageComponentName> = {
  TemplateListPage: ZwTemplateListPage,
  TemplateListEditPage:ZwTemplateListEditPage,
}

export { templatePageMap };