/*
 * @Description: 全局注册低代码调用的组件
 * @Author: zhangwei
 * @Date: 2023-03-14 22:37:32
 * @LastEditTime: 2023-03-18 08:16:17
 * @LastEditors: zhangwei
 */
import type { App } from "vue";

import { ContentPage } from "./content-page"
import { ZwModal } from "./dialog"

// 表单相关
import {formCmpMap} from "./form";
import { ZwSearch } from './form/search'
import { ZwForm } from "./form/form"
import { ZwDescription } from "./form/description"

//模板页面
import {templatePageMap} from "./template"

export const setupLowcodeCom = (app: App<Element>): void => {

  app.component("ContentPage", ContentPage);
  app.component("ZwModal",ZwModal),

  //form
  app.component("ZwInput", formCmpMap.ZwInput);
  app.component("ZwSelect", formCmpMap.ZwSelect);
  app.component("ZwTreeSelect", formCmpMap.ZwTreeSelect);

  app.component("ZwForm", ZwForm);
  app.component("ZwDescription", ZwDescription);
  app.component("ZwSearch", ZwSearch);
  app.component("ZwButton", formCmpMap.ZwButton);
  app.component("ZwTextButton", formCmpMap.ZwTextButton);
  app.component("ZwLabel", formCmpMap.ZwLabel);
  app.component("ZwCheckbox", formCmpMap.ZwCheckbox);


  //template
  app.component("ZwTemplateListPage", templatePageMap.TemplateListPage)
  app.component("ZwTemplateListEditPage", templatePageMap.TemplateListEditPage)
  
};