/*
 * @Description: 
 * @Author: zhangwei
 * @Date: 2023-03-15 20:31:33
 * @LastEditTime: 2023-03-20 07:01:42
 * @LastEditors: zw 775925302@qq.com
 */

//表单schema结构
export interface FormSchema {
    title:string
    data:Recordable
    attrs:Recordable
    widgets:any[]
}

//////////////////////////////////////////template

export interface PageListSchema{
    isInDialog? : boolean //是否在对话框中
    searchConfig?:Recordable //搜索配置
    tableConfig: Recordable //表格配置
    proxyConfig: Recordable //接口配置
    detailConfig?:FormSchema //详情配置
    editConfig?: FormSchema //编辑配置
    createConfig?:FormSchema //新增配置
    formModelWidth?:string,
    formModelHeight?:string,
}


export interface PageListEditSchema{
    isInDialog? : boolean //是否在对话框中
    searchConfig?:Recordable //搜索配置
    tableConfig: Recordable //表格配置
    proxyConfig: Recordable //接口配置
}

///////////////////////////////////request
export interface RequestActionParamsDefault{
    key:string,
    value:any,
}

export interface RequestActionParamsRouter{
    key:string,  //需要上传的参数
    routerKey:string, //从路由取的key
}

export interface RequestActionParams{
    default:RequestActionParamsDefault[],
    router:RequestActionParamsRouter[],
}

export interface RequestAction{
    url:string
    method:string
    params?:RequestActionParams
}
