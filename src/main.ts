/*
 * @Description: 
 * @Author: zhangwei
 * @Date: 2023-03-03 22:58:31
 * @LastEditTime: 2023-03-08 16:08:09
 * @LastEditors: zhangwei
 */
// 引入windi css
import '@/plugins/windi.css'

// 导入全局的svg图标
import '@/plugins/svgIcon'

// 初始化多语言
import { setupI18n } from '@/plugins/vueI18n'

// 引入状态管理
import { setupStore } from '@/store'

// 全局组件
import { setupGlobCom } from '@/components'
import { setupLowcodeCom } from "@/components-lowcode"

// 引入全局样式
import '@/styles/index.less'

// 引入动画
import '@/plugins/animate.css'

// 路由
import { setupRouter } from './router'

// 权限
import { setupPermission } from './directives'

import { createApp } from 'vue'

import App from './App.vue'

import './permission'

import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'

// 创建实例
const setupAll = async () => {
  const app = createApp(App)

  await setupI18n(app)

  app.use(VXETable)
  
  setupStore(app)

  setupGlobCom(app)
  setupLowcodeCom(app)

  setupRouter(app)

  setupPermission(app)

  app.mount('#app')
}

setupAll()
