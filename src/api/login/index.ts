import request from '@/config/axios'
import type { UserLoginVO } from './types'

export const loginApi = (data: UserLoginVO) => {
  return request.post({ url: '/system/auth/login', data })
}

// 登出
export const loginOutApi = () => {
  return request.post({ url: '/system/auth/logout' })
}

// 获取用户权限信息
export const getInfoApi = () => {
  return request.get({ url: '/system/auth/get-permission-info' })
}


// 路由
export const getAsyncRoutesApi = () => {
  return request.get({ url: '/system/auth/list-menus' })
}
